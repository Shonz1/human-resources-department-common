import {EducationEntity, EducationId} from "../../../entities/education.entity";
import {PersonId} from "../../../entities/person.entity";

export const FindEducationQuerySymbol = Symbol('FindEducationQuery');

export interface FindEducationQuery {
    findEducationById(educationId: EducationId): Promise<EducationEntity | null>;
    findEducationsByPersonId(personId: PersonId): Promise<EducationEntity[]>;
}
