import {CompanyEntity, CompanyId} from "../../../entities/company.entity";

export const FindCompanyQuerySymbol = Symbol('FindCompanyQuery');

export interface FindCompanyQuery {
    findCompanyById(companyId: CompanyId): Promise<CompanyEntity | null>;
    findCompanyByName(name: string): Promise<CompanyEntity | null>;
    findAllCompanies(): Promise<CompanyEntity[]>;
}
