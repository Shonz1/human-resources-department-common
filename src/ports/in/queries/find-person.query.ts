import {PersonEntity, PersonId} from "../../../entities/person.entity";

export const FindPersonQuerySymbol = Symbol('FindPersonQuery');

export interface FindPersonQuery {
    findPersonById(personId: PersonId): Promise<PersonEntity | null>;
    findPersonByFullName(lastName: string, firstName: string, patronymic: string): Promise<PersonEntity | null>;
    findPeopleOlderThan(age: number): Promise<PersonEntity[]>;
    findAllPeople(): Promise<PersonEntity[]>;
}
