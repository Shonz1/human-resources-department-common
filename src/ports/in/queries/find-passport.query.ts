import {PassportEntity, PassportId} from "../../../entities/passport.entity";
import {PersonId} from "../../../entities/person.entity";

export const FindPassportQuerySymbol = Symbol('FindPassportQuery');

export interface FindPassportQuery {
    findPassportById(passportId: PassportId): Promise<PassportEntity | null>;
    findPassportBySerialAndNumber(serial: string, number: string): Promise<PassportEntity | null>;
    findPassportsByPersonId(personId: PersonId): Promise<PassportEntity[]>;
    findAllPassports(): Promise<PassportEntity[]>;
}
