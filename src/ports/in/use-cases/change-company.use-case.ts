import {ChangeCompanyDirectorCommand} from "../commands/change-company-director.command";

export const ChangeCompanyUseCaseSymbol = Symbol('ChangeCompanyUseCase');

export interface ChangeCompanyUseCase {
    changeCompanyDirector(command: ChangeCompanyDirectorCommand): Promise<void>;
}
