import {CreatePersonCommand} from "../commands/create-person.command";
import {PersonEntity} from "../../../entities/person.entity";

export const CreatePersonUseCaseSymbol = Symbol('CreatePersonUseCase');

export interface CreatePersonUseCase {
    createPerson(command: CreatePersonCommand): Promise<PersonEntity>;
}
