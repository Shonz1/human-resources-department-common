import {RemoveCompanyCommand} from "../commands/remove-company.command";

export const RemoveCompanyUseCaseSymbol = Symbol('RemoveCompanyUseCase');

export interface RemoveCompanyUseCase {
    removeCompany(command: RemoveCompanyCommand): Promise<void>;
}
