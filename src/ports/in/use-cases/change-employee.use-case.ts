import {ChangeEmployeeRoleCommand} from "../commands/change-employee-role.command";
import {ChangeEmployeeSalaryCommand} from "../commands/change-employee-salary.command";
import {ChangeEmployeeSocialPrivilegeCommand} from "../commands/change-employee-social-privilege.command";
import {ChangeEmployeeFiredAtCommand} from "../commands/change-employee-fired-at.command";

export const ChangeEmployeeUseCaseSymbol = Symbol('ChangeEmployeeUseCase');

export interface ChangeEmployeeUseCase {
    changeEmployeeRole(command: ChangeEmployeeRoleCommand): Promise<void>;
    changeEmployeeSalary(command: ChangeEmployeeSalaryCommand): Promise<void>;
    changeEmployeeSocialPrivilege(command: ChangeEmployeeSocialPrivilegeCommand): Promise<void>;
    changeEmployeeFiredAt(command: ChangeEmployeeFiredAtCommand): Promise<void>;
}
