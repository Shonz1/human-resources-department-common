import {CreatePassportCommand} from "../commands/create-passport.command";
import {PassportEntity} from "../../../entities/passport.entity";

export const CreatePassportUseCaseSymbol = Symbol('CreatePassportUseCase');

export interface CreatePassportUseCase {
    createPassport(command: CreatePassportCommand): Promise<PassportEntity>;
}
