import {RemovePassportCommand} from "../commands/remove-passport.command";

export const RemovePassportUseCaseSymbol = Symbol('RemovePassportUseCase');

export interface RemovePassportUseCase {
    removePassport(command: RemovePassportCommand): Promise<void>;
}
