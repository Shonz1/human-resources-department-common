import {CreateEmployeeCommand} from "../commands/create-employee.command";
import {EmployeeEntity} from "../../../entities/employee.entity";

export const CreateEmployeeUseCaseSymbol = Symbol('CreateEmployeeUseCase');

export interface CreateEmployeeUseCase {
    createEmployee(command: CreateEmployeeCommand): Promise<EmployeeEntity>;
}
