import {RemoveEmployeeCommand} from "../commands/remove-employee.command";

export const RemoveEmployeeUseCaseSymbol = Symbol('RemoveEmployeeUseCase');

export interface RemoveEmployeeUseCase {
    removeEmployee(command: RemoveEmployeeCommand): Promise<void>;
}
