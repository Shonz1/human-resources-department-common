import {CreateEducationCommand} from "../commands/create-education.command";
import {EducationEntity} from "../../../entities/education.entity";

export const CreateEducationUseCaseSymbol = Symbol('CreateEducationUseCase');

export interface CreateEducationUseCase {
    createEducation(command: CreateEducationCommand): Promise<EducationEntity>;
}
