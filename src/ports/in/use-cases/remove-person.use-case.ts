import {RemovePersonCommand} from "../commands/remove-person.command";

export const RemovePersonUseCaseSymbol = Symbol('RemovePersonUseCase');

export interface RemovePersonUseCase {
    removePerson(command: RemovePersonCommand): Promise<void>;
}
