import {RemoveEducationCommand} from "../commands/remove-education.command";

export const RemoveEducationUseCaseSymbol = Symbol('RemoveEducationUseCase');

export interface RemoveEducationUseCase {
    removeEducation(command: RemoveEducationCommand): Promise<void>;
}
