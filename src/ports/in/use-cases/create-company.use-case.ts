import {CreateCompanyCommand} from "../commands/create-company.command";
import {CompanyEntity} from "../../../entities/company.entity";

export const CreateCompanyUseCaseSymbol = Symbol('CreateCompanyUseCase');

export interface CreateCompanyUseCase {
    createCompany(command: CreateCompanyCommand): Promise<CompanyEntity>;
}
