import {EmployeeId} from "../../../entities/employee.entity";

export class ChangeEmployeeSalaryCommand {
    constructor(
        private readonly _employeeId: EmployeeId,
        private readonly _salary: number,
    ) {}

    get employeeId(): EmployeeId {
        return this._employeeId;
    }

    get salary(): number {
        return this._salary;
    }
}
