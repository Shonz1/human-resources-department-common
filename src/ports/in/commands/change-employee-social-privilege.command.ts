import {EmployeeId} from "../../../entities/employee.entity";

export class ChangeEmployeeSocialPrivilegeCommand {
    constructor(
        private readonly _employeeId: EmployeeId,
        private readonly _socialPrivilege: boolean,
    ) {}

    get employeeId(): EmployeeId {
        return this._employeeId;
    }

    get socialPrivilege(): boolean {
        return this._socialPrivilege;
    }
}
