import {PersonId} from "../../../entities/person.entity";
import {CompanyId} from "../../../entities/company.entity";

export class CreateEmployeeCommand {
    constructor(
        private readonly _personId: PersonId,
        private readonly _companyId: CompanyId,
        private readonly _role: string,
        private readonly _salary: number,
        private readonly _socialPrivilege: boolean,
    ) {}

    get personId(): PersonId {
        return this._personId;
    }

    get companyId(): CompanyId {
        return this._companyId;
    }

    get role(): string {
        return this._role;
    }

    get salary(): number {
        return this._salary;
    }

    get socialPrivilege(): boolean {
        return this._socialPrivilege;
    }
}
