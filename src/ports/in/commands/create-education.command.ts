import {EducationLevel} from "../../../entities/education.entity";
import {PersonId} from "../../../entities/person.entity";

export class CreateEducationCommand {
    constructor(
        private readonly _personId: PersonId,
        private readonly _name: string,
        private readonly _level: EducationLevel,
    ) {}

    get personId(): PersonId {
        return this._personId;
    }

    get name(): string {
        return this._name;
    }

    get level(): EducationLevel {
        return this._level;
    }
}
