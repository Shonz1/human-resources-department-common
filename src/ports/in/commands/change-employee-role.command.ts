import {EmployeeId} from "../../../entities/employee.entity";

export class ChangeEmployeeRoleCommand {
    constructor(
        private readonly _employeeId: EmployeeId,
        private readonly _role: string,
    ) {}

    get employeeId(): EmployeeId {
        return this._employeeId;
    }

    get role(): string {
        return this._role;
    }
}
