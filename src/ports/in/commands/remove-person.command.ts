import {PersonId} from "../../../entities/person.entity";

export class RemovePersonCommand {
    constructor(
        private readonly _personId: PersonId,
    ) {}

    get personId(): PersonId {
        return this._personId;
    }
}
