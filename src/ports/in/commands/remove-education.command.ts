import {EducationId} from "../../../entities/education.entity";

export class RemoveEducationCommand {
    constructor(
        private readonly _educationId: EducationId,
    ) {}

    get educationId(): EducationId {
        return this._educationId;
    }
}
