import {EmployeeId} from "../../../entities/employee.entity";

export class RemoveEmployeeCommand {
    constructor(
        private readonly _employeeId: EmployeeId,
    ) {}

    get employeeId(): EmployeeId {
        return this._employeeId;
    }
}
