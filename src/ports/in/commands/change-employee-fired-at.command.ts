import {EmployeeId} from "../../../entities/employee.entity";

export class ChangeEmployeeFiredAtCommand {
    constructor(
        private readonly _employeeId: EmployeeId,
        private readonly _firedAt: Date,
    ) {}

    get employeeId(): EmployeeId {
        return this._employeeId;
    }

    get firedAt(): Date {
        return this._firedAt;
    }
}
