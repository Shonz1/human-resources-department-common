import {EmployeeId} from "../../../entities/employee.entity";
import {CompanyId} from "../../../entities/company.entity";

export class ChangeCompanyDirectorCommand {
    constructor(
        private readonly _companyId: CompanyId,
        private readonly _directorId: EmployeeId,
    ) {}

    get companyId(): CompanyId {
        return this._companyId;
    }

    get directorId(): EmployeeId {
        return this._directorId;
    }
}
