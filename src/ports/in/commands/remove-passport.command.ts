import {PassportId} from "../../../entities/passport.entity";

export class RemovePassportCommand {
    constructor(
        private readonly _passportId: PassportId,
    ) {}

    get passportId(): PassportId {
        return this._passportId;
    }
}
