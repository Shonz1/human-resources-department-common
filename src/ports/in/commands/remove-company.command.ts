import {CompanyId} from "../../../entities/company.entity";

export class RemoveCompanyCommand {
    constructor(
        private readonly _companyId: CompanyId,
    ) {}

    get companyId(): CompanyId {
        return this._companyId;
    }
}
