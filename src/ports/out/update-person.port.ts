import {PersonEntity} from "../../entities/person.entity";

export interface UpdatePersonPort {
    createPerson(person: PersonEntity): Promise<PersonEntity>;
    removePerson(person: PersonEntity): Promise<void>;
}
