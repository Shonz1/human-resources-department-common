import {EducationEntity} from "../../entities/education.entity";

export interface UpdateEducationPort {
    createEducation(education: EducationEntity): Promise<EducationEntity>;
    removeEducation(education: EducationEntity): Promise<void>;
}
