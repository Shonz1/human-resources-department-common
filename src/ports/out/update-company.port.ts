import {CompanyEntity} from "../../entities/company.entity";

export interface UpdateCompanyPort {
    createCompany(company: CompanyEntity): Promise<CompanyEntity>;
    removeCompany(company: CompanyEntity): Promise<void>;
    changeCompanyDirector(company: CompanyEntity): Promise<void>;
}
