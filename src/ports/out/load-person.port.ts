import {PersonEntity, PersonId} from "../../entities/person.entity";

export interface LoadPersonPort {
    findPersonById(personId: PersonId): Promise<PersonEntity | null>;
    findPersonByFullName(lastName: string, firstName: string, patronymic: string): Promise<PersonEntity | null>;
    findPeopleOlderThan(age: number): Promise<PersonEntity[]>;
    findAllPeople(): Promise<PersonEntity[]>;
}
