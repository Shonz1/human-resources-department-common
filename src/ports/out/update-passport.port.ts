import {PassportEntity} from "../../entities/passport.entity";

export interface UpdatePassportPort {
    createPassport(passport: PassportEntity): Promise<PassportEntity>;
    removePassport(passport: PassportEntity): Promise<void>;
}
