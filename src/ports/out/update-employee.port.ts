import {EmployeeEntity} from "../../entities/employee.entity";

export interface UpdateEmployeePort {
    createEmployee(employee: EmployeeEntity): Promise<EmployeeEntity>;
    removeEmployee(employee: EmployeeEntity): Promise<void>;
    changeEmployeeRole(employee: EmployeeEntity): Promise<void>;
    changeEmployeeSalary(employee: EmployeeEntity): Promise<void>;
    changeEmployeeSocialPrivilege(employee: EmployeeEntity): Promise<void>;
    changeEmployeeFiredAt(employee: EmployeeEntity): Promise<void>;
}
