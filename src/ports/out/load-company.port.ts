import {CompanyEntity, CompanyId} from "../../entities/company.entity";

export interface LoadCompanyPort {
    findCompanyById(companyId: CompanyId): Promise<CompanyEntity | null>;
    findCompanyByName(name: string): Promise<CompanyEntity | null>;
    findAllCompanies(): Promise<CompanyEntity[]>;
}
