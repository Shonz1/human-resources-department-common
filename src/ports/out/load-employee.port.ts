import {EmployeeEntity, EmployeeId} from "../../entities/employee.entity";
import {CompanyId} from "../../entities/company.entity";
import {PersonId} from "../../entities/person.entity";

export interface LoadEmployeePort {
    findEmployeeById(employeeId: EmployeeId): Promise<EmployeeEntity | null>;
    findEmployeeByPersonIdAndCompanyId(personId: PersonId, companyId: CompanyId): Promise<EmployeeEntity | null>;
    findEmployeesByPersonId(personId: PersonId): Promise<EmployeeEntity[]>;
    findEmployeesByCompanyId(companyId: CompanyId): Promise<EmployeeEntity[]>;
    findEmployeesOlderThan(age: number): Promise<EmployeeEntity[]>;
    findAllEmployees(): Promise<EmployeeEntity[]>;
}
