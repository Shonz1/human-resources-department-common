import {EducationEntity, EducationId} from "../../entities/education.entity";
import {PersonId} from "../../entities/person.entity";

export interface LoadEducationPort {
    findEducationById(educationId: EducationId): Promise<EducationEntity | null>;
    findEducationsByPersonId(personId: PersonId): Promise<EducationEntity[]>;
}
