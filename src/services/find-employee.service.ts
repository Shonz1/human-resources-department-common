import {FindEmployeeQuery} from "../ports/in/queries/find-employee.query";
import {LoadEmployeePort} from "../ports/out/load-employee.port";
import {EmployeeEntity, EmployeeId} from "../entities/employee.entity";
import {CompanyId} from "../entities/company.entity";
import {PersonId} from "../entities/person.entity";

export class FindEmployeeService implements FindEmployeeQuery {
    constructor(
        private readonly _loadEmployeePort: LoadEmployeePort,
    ) {}

    async findEmployeeById(employeeId: EmployeeId): Promise<EmployeeEntity | null> {
        return await this._loadEmployeePort.findEmployeeById(employeeId);
    }

    async findEmployeeByPersonIdAndCompanyId(personId: PersonId, companyId: CompanyId): Promise<EmployeeEntity | null> {
        return await this._loadEmployeePort.findEmployeeByPersonIdAndCompanyId(personId, companyId);
    }

    async findEmployeesByPersonId(personId: PersonId): Promise<EmployeeEntity[]> {
        return await this._loadEmployeePort.findEmployeesByPersonId(personId);
    }

    async findEmployeesByCompanyId(companyId: CompanyId): Promise<EmployeeEntity[]> {
        return await this._loadEmployeePort.findEmployeesByCompanyId(companyId);
    }

    async findEmployeesOlderThan(age: number): Promise<EmployeeEntity[]> {
        return await this._loadEmployeePort.findEmployeesOlderThan(age);
    }

    async findAllEmployees(): Promise<EmployeeEntity[]> {
        return await this._loadEmployeePort.findAllEmployees();
    }
}
