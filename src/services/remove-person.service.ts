import {RemovePersonUseCase} from "../ports/in/use-cases/remove-person.use-case";
import {LoadPersonPort} from "../ports/out/load-person.port";
import {UpdatePersonPort} from "../ports/out/update-person.port";
import {RemovePersonCommand} from "../ports/in/commands/remove-person.command";

export class RemovePersonService implements RemovePersonUseCase {
    constructor(
        private readonly _loadPersonPort: LoadPersonPort,
        private readonly _updatePersonPort: UpdatePersonPort,
    ) {}

    async removePerson(command: RemovePersonCommand): Promise<void> {
        const person = await this._loadPersonPort.findPersonById(command.personId);
        if (!person)
            return;

        return await this._updatePersonPort.removePerson(person);
    }
}
