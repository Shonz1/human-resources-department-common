import {LoadCompanyPort} from "../ports/out/load-company.port";
import {UpdateCompanyPort} from "../ports/out/update-company.port";
import {CreateCompanyUseCase} from "../ports/in/use-cases/create-company.use-case";
import {CreateCompanyCommand} from "../ports/in/commands/create-company.command";
import {CompanyEntity} from "../entities/company.entity";

export class CreateCompanyService implements CreateCompanyUseCase {
    constructor(
        private readonly _loadCompanyPort: LoadCompanyPort,
        private readonly _updateCompanyPort: UpdateCompanyPort,
    ) {}

    async createCompany(command: CreateCompanyCommand): Promise<CompanyEntity> {
        let company = await this._loadCompanyPort.findCompanyById(command.id);
        if (company)
            throw new Error('COMPANY_ALREADY_EXISTS');

        return await this._updateCompanyPort.createCompany(
            new CompanyEntity(
                command.id,
                command.shortName,
                command.fullName,
                command.internationalName,
                command.address,
                command.phone,
            )
        );
    }
}
