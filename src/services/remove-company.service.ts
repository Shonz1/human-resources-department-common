import {LoadCompanyPort} from "../ports/out/load-company.port";
import {UpdateCompanyPort} from "../ports/out/update-company.port";
import {RemoveCompanyUseCase} from "../ports/in/use-cases/remove-company.use-case";
import {RemoveCompanyCommand} from "../ports/in/commands/remove-company.command";
import {LoadEmployeePort} from "../ports/out/load-employee.port";
import {UpdateEmployeePort} from "../ports/out/update-employee.port";

export class RemoveCompanyService implements RemoveCompanyUseCase {
    constructor(
        private readonly _loadCompanyPort: LoadCompanyPort,
        private readonly _updateCompanyPort: UpdateCompanyPort,
        private readonly _loadEmployeePort: LoadEmployeePort,
        private readonly _updateEmployeePort: UpdateEmployeePort,
    ) {}

    async removeCompany(command: RemoveCompanyCommand): Promise<void> {
        const company = await this._loadCompanyPort.findCompanyById(command.companyId);
        if (!company)
            return;

        const employees = await this._loadEmployeePort.findEmployeesByCompanyId(company.id);
        for (const employee of employees)
            await this._updateEmployeePort.removeEmployee(employee);

        return await this._updateCompanyPort.removeCompany(company);
    }
}
