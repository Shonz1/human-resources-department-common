import {ChangeEmployeeUseCase} from "../ports/in/use-cases/change-employee.use-case";
import {LoadEmployeePort} from "../ports/out/load-employee.port";
import {UpdateEmployeePort} from "../ports/out/update-employee.port";
import {ChangeEmployeeRoleCommand} from "../ports/in/commands/change-employee-role.command";
import {ChangeEmployeeSalaryCommand} from "../ports/in/commands/change-employee-salary.command";
import {ChangeEmployeeSocialPrivilegeCommand} from "../ports/in/commands/change-employee-social-privilege.command";
import {EmployeeEntity} from "../entities/employee.entity";
import {ChangeEmployeeFiredAtCommand} from "../ports/in/commands/change-employee-fired-at.command";

export class ChangeEmployeeService implements ChangeEmployeeUseCase {
    constructor(
        private readonly _loadEmployeePort: LoadEmployeePort,
        private readonly _updateEmployeePort: UpdateEmployeePort,
    ) {}

    async changeEmployeeRole(command: ChangeEmployeeRoleCommand): Promise<void> {
        const employee = await this._loadEmployeePort.findEmployeeById(command.employeeId);
        if (!employee)
            throw new Error('EMPLOYEE_DOESNT_EXISTS');

        return await this._updateEmployeePort.changeEmployeeRole(
            new EmployeeEntity(
                employee.id,
                employee.personId,
                employee.companyId,
                command.role,
                employee.salary,
                employee.socialPrivilege,
                employee.employmentAt,
                employee.firedAt,
            )
        );
    }

    async changeEmployeeSalary(command: ChangeEmployeeSalaryCommand): Promise<void> {
        const employee = await this._loadEmployeePort.findEmployeeById(command.employeeId);
        if (!employee)
            throw new Error('EMPLOYEE_DOESNT_EXISTS');

        return await this._updateEmployeePort.changeEmployeeSalary(
            new EmployeeEntity(
                employee.id,
                employee.personId,
                employee.companyId,
                employee.role,
                command.salary,
                employee.socialPrivilege,
                employee.employmentAt,
                employee.firedAt,
            )
        );
    }

    async changeEmployeeSocialPrivilege(command: ChangeEmployeeSocialPrivilegeCommand): Promise<void> {
        const employee = await this._loadEmployeePort.findEmployeeById(command.employeeId);
        if (!employee)
            throw new Error('EMPLOYEE_DOESNT_EXISTS');

        return await this._updateEmployeePort.changeEmployeeSocialPrivilege(
            new EmployeeEntity(
                employee.id,
                employee.personId,
                employee.companyId,
                employee.role,
                employee.salary,
                command.socialPrivilege,
                employee.employmentAt,
                employee.firedAt,
            )
        );
    }

    async changeEmployeeFiredAt(command: ChangeEmployeeFiredAtCommand): Promise<void> {
        const employee = await this._loadEmployeePort.findEmployeeById(command.employeeId);
        if (!employee)
            throw new Error('EMPLOYEE_DOESNT_EXISTS');

        return await this._updateEmployeePort.changeEmployeeFiredAt(
            new EmployeeEntity(
                employee.id,
                employee.personId,
                employee.companyId,
                employee.role,
                employee.salary,
                employee.socialPrivilege,
                employee.employmentAt,
                command.firedAt,
            )
        );
    }
}
