import {ChangeCompanyUseCase} from "../ports/in/use-cases/change-company.use-case";
import {LoadCompanyPort} from "../ports/out/load-company.port";
import {UpdateCompanyPort} from "../ports/out/update-company.port";
import {LoadEmployeePort} from "../ports/out/load-employee.port";
import {ChangeCompanyDirectorCommand} from "../ports/in/commands/change-company-director.command";
import {CompanyEntity} from "../entities/company.entity";

export class ChangeCompanyService implements ChangeCompanyUseCase {
    constructor(
        private readonly _loadCompanyPort: LoadCompanyPort,
        private readonly _updateCompanyPort: UpdateCompanyPort,
        private readonly _loadEmployeePort: LoadEmployeePort,
    ) {}

    async changeCompanyDirector(command: ChangeCompanyDirectorCommand): Promise<void> {
        const company = await this._loadCompanyPort.findCompanyById(command.companyId);
        if (!company)
            throw new Error('COMPANY_DOESNT_EXISTS');

        const employee = await this._loadEmployeePort.findEmployeeById(command.directorId);
        if (!employee)
            throw new Error('EMPLOYEE_DOESNT_EXISTS');

        if (employee.companyId !== company.id)
            throw new Error('THIS_EMPLOYEE_BELONGS_ANOTHER_COMPANY');

        return await this._updateCompanyPort.changeCompanyDirector(
            new CompanyEntity(
                company.id,
                company.shortName,
                company.fullName,
                company.internationalName,
                company.address,
                company.phone,
                command.directorId,
            )
        );
    }
}
