import {RemovePassportUseCase} from "../ports/in/use-cases/remove-passport.use-case";
import {LoadPassportPort} from "../ports/out/load-passport.port";
import {UpdatePassportPort} from "../ports/out/update-passport.port";
import {RemovePassportCommand} from "../ports/in/commands/remove-passport.command";

export class RemovePassportService implements RemovePassportUseCase {
    constructor(
        private readonly _loadPassportPort: LoadPassportPort,
        private readonly _updatePassportPort: UpdatePassportPort,
    ) {}

    async removePassport(command: RemovePassportCommand): Promise<void> {
        const passport = await this._loadPassportPort.findPassportById(command.passportId);
        if (!passport)
            return;

        return await this._updatePassportPort.removePassport(passport);
    }
}
