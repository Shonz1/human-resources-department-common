import {LoadPersonPort} from "../ports/out/load-person.port";
import {FindPersonQuery} from "../ports/in/queries/find-person.query";
import {PersonEntity, PersonId} from "../entities/person.entity";

export class FindPersonService implements FindPersonQuery {
    constructor(
        private readonly _loadPersonPort: LoadPersonPort
    ) {}

    async findPersonById(personId: PersonId): Promise<PersonEntity | null> {
        return await this._loadPersonPort.findPersonById(personId);
    }

    async findPersonByFullName(firstName: string, lastName: string, patronymic: string): Promise<PersonEntity | null> {
        return await this._loadPersonPort.findPersonByFullName(firstName, lastName, patronymic);
    }

    async findPeopleOlderThan(age: number): Promise<PersonEntity[]> {
        return await this._loadPersonPort.findPeopleOlderThan(age);
    }

    async findAllPeople(): Promise<PersonEntity[]> {
        return await this._loadPersonPort.findAllPeople();
    }
}
