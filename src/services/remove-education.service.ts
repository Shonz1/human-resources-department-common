import {RemoveEducationUseCase} from "../ports/in/use-cases/remove-education.use-case";
import {UpdateEducationPort} from "../ports/out/update-education.port";
import {LoadEducationPort} from "../ports/out/load-education.port";
import {RemoveEducationCommand} from "../ports/in/commands/remove-education.command";

export class RemoveEducationService implements RemoveEducationUseCase {
    constructor(
        private readonly _loadEducationPort: LoadEducationPort,
        private readonly _updateEducationPort: UpdateEducationPort,
    ) {}

    async removeEducation(command: RemoveEducationCommand): Promise<void> {
        const education = await this._loadEducationPort.findEducationById(command.educationId);
        if (!education)
            return;

        return await this._updateEducationPort.removeEducation(education);
    }
}
