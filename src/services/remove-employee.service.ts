import {RemoveEmployeeUseCase} from "../ports/in/use-cases/remove-employee.use-case";
import {LoadEmployeePort} from "../ports/out/load-employee.port";
import {UpdateEmployeePort} from "../ports/out/update-employee.port";
import {RemoveEmployeeCommand} from "../ports/in/commands/remove-employee.command";

export class RemoveEmployeeService implements RemoveEmployeeUseCase {
    constructor(
        private readonly _loadEmployeePort: LoadEmployeePort,
        private readonly _updateEmployeePort: UpdateEmployeePort,
    ) {}

    async removeEmployee(command: RemoveEmployeeCommand): Promise<void> {
        const employee = await this._loadEmployeePort.findEmployeeById(command.employeeId);
        if (!employee)
            return;

        return await this._updateEmployeePort.removeEmployee(employee);
    }
}
