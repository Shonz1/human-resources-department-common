import {FindPassportQuery} from "../ports/in/queries/find-passport.query";
import {LoadPassportPort} from "../ports/out/load-passport.port";
import {PassportEntity, PassportId} from "../entities/passport.entity";
import {PersonId} from "../entities/person.entity";

export class FindPassportService implements FindPassportQuery {
    constructor(
        private readonly _loadPassportPort: LoadPassportPort,
    ) {}

    async findPassportById(passportId: PassportId): Promise<PassportEntity | null> {
        return await this._loadPassportPort.findPassportById(passportId);
    }

    async findPassportBySerialAndNumber(serial: string, number: string): Promise<PassportEntity | null> {
        return await this._loadPassportPort.findPassportBySerialAndNumber(serial, number);
    }

    async findPassportsByPersonId(personId: PersonId): Promise<PassportEntity[]> {
        return await this._loadPassportPort.findPassportsByPersonId(personId);
    }

    async findAllPassports(): Promise<PassportEntity[]> {
        return await this._loadPassportPort.findAllPassports();
    }
}
