import {LoadCompanyPort} from "../ports/out/load-company.port";
import {FindCompanyQuery} from "../ports/in/queries/find-company.query";
import {CompanyEntity, CompanyId} from "../entities/company.entity";

export class FindCompanyService implements FindCompanyQuery {
    constructor(
        private readonly _loadCompanyPort: LoadCompanyPort,
    ) {}

    async findCompanyById(companyId: CompanyId): Promise<CompanyEntity | null> {
        return await this._loadCompanyPort.findCompanyById(companyId);
    }

    async findCompanyByName(name: string): Promise<CompanyEntity | null> {
        return await this._loadCompanyPort.findCompanyByName(name);
    }

    async findAllCompanies(): Promise<CompanyEntity[]> {
        return await this._loadCompanyPort.findAllCompanies();
    }
}
