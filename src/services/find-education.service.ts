import {FindEducationQuery} from "../ports/in/queries/find-education.query";
import {LoadEducationPort} from "../ports/out/load-education.port";
import {EducationEntity, EducationId} from "../entities/education.entity";
import {PersonId} from "../entities/person.entity";

export class FindEducationService implements FindEducationQuery {
    constructor(
        private readonly _loadEducationPort: LoadEducationPort,
    ) {}

    async findEducationById(educationId: EducationId): Promise<EducationEntity | null> {
        return await this._loadEducationPort.findEducationById(educationId);
    }

    async findEducationsByPersonId(personId: PersonId): Promise<EducationEntity[]> {
        return await this._loadEducationPort.findEducationsByPersonId(personId);
    }
}
