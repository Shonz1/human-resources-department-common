import {CreateEmployeeUseCase} from "../ports/in/use-cases/create-employee.use-case";
import {LoadEmployeePort} from "../ports/out/load-employee.port";
import {UpdateEmployeePort} from "../ports/out/update-employee.port";
import {CreateEmployeeCommand} from "../ports/in/commands/create-employee.command";
import {EmployeeEntity} from "../entities/employee.entity";

export class CreateEmployeeService implements CreateEmployeeUseCase {
    constructor(
        private readonly _loadEmployeePort: LoadEmployeePort,
        private readonly _updateEmployeePort: UpdateEmployeePort,
    ) {}

    async createEmployee(command: CreateEmployeeCommand): Promise<EmployeeEntity> {
        const employee = await this._loadEmployeePort.findEmployeeByPersonIdAndCompanyId(command.personId, command.companyId);
        if (employee)
            throw new Error('EMPLOYEE_ALREADY_EXISTS');

        return await this._updateEmployeePort.createEmployee(
            new EmployeeEntity(
                null,
                command.personId,
                command.companyId,
                command.role,
                command.salary,
                command.socialPrivilege,
                new Date()
            )
        );
    }
}
