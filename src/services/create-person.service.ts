import {CreatePersonUseCase} from "../ports/in/use-cases/create-person.use-case";
import {LoadPersonPort} from "../ports/out/load-person.port";
import {UpdatePersonPort} from "../ports/out/update-person.port";
import {CreatePersonCommand} from "../ports/in/commands/create-person.command";
import {PersonEntity} from "../entities/person.entity";

export class CreatePersonService implements CreatePersonUseCase {
    constructor(
        private readonly _loadPersonPort: LoadPersonPort,
        private readonly _updatePersonPort: UpdatePersonPort,
    ) {}

    async createPerson(command: CreatePersonCommand): Promise<PersonEntity> {
        const person = await this._loadPersonPort.findPersonById(command.id);
        if (person)
            throw new Error('PERSON_ALREADY_EXISTS');

        return await this._updatePersonPort.createPerson(
            new PersonEntity(
                command.id,
                command.lastName,
                command.firstName,
                command.patronymic,
                command.gender,
                command.birthday,
            )
        );
    }
}
