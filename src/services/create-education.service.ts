import {CreateEducationUseCase} from "../ports/in/use-cases/create-education.use-case";
import {UpdateEducationPort} from "../ports/out/update-education.port";
import {CreateEducationCommand} from "../ports/in/commands/create-education.command";
import {EducationEntity} from "../entities/education.entity";

export class CreateEducationService implements CreateEducationUseCase {
    constructor(
        private readonly _updateEducationPort: UpdateEducationPort,
    ) {}

    async createEducation(command: CreateEducationCommand): Promise<EducationEntity> {
        return await this._updateEducationPort.createEducation(
            new EducationEntity(
                null,
                command.personId,
                command.name,
                command.level,
            )
        );
    }
}
