import {CreatePassportUseCase} from "../ports/in/use-cases/create-passport.use-case";
import {LoadPassportPort} from "../ports/out/load-passport.port";
import {UpdatePassportPort} from "../ports/out/update-passport.port";
import {CreatePassportCommand} from "../ports/in/commands/create-passport.command";
import {PassportEntity} from "../entities/passport.entity";

export class CreatePassportService implements CreatePassportUseCase {
    constructor(
        private readonly _loadPassportPort: LoadPassportPort,
        private readonly _updatePassportPort: UpdatePassportPort,
    ) {}

    async createPassport(command: CreatePassportCommand): Promise<PassportEntity> {
        const passport = await this._loadPassportPort.findPassportById(command.serial + command.number);
        if (passport)
            throw new Error('PASSPORT_ALREADY_EXISTS');

        return await this._updatePassportPort.createPassport(
            new PassportEntity(
                command.serial + command.number,
                command.personId,
                command.serial,
                command.number,
                command.issuer,
                command.issuedAt,
            )
        );
    }
}
