import {PersonId} from "./person.entity";
import {CompanyId} from "./company.entity";

export type EmployeeId = string;

export class EmployeeEntity {
    constructor(
        private readonly _id: EmployeeId,
        private readonly _personId: PersonId,
        private readonly _companyId: CompanyId,
        private readonly _role: string,
        private readonly _salary: number,
        private readonly _socialPrivilege: boolean,
        private readonly _employmentAt: Date,
        private readonly _firedAt?: Date,
    ) {}

    get id(): EmployeeId {
        return this._id;
    }

    get personId(): PersonId {
        return this._personId;
    }

    get companyId(): CompanyId {
        return this._companyId;
    }

    get role(): string {
        return this._role;
    }

    get salary(): number {
        return this._salary;
    }

    get socialPrivilege(): boolean {
        return this._socialPrivilege;
    }

    get employmentAt(): Date {
        return this._employmentAt;
    }

    get firedAt(): Date {
        return this._firedAt;
    }
}
