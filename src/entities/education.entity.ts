import {PersonId} from "./person.entity";

export enum EducationLevel {
    middle,
    special,
    higher,
}

export type EducationId = string;

export class EducationEntity {
    constructor(
        private readonly _id: EducationId,
        private readonly _personId: PersonId,
        private readonly _name: string,
        private readonly _level: EducationLevel,
    ) {}

    get id(): EducationId {
        return this._id;
    }

    get personId(): PersonId {
        return this._personId;
    }

    get name(): string {
        return this._name;
    }

    get level(): EducationLevel {
        return this._level;
    }
}
