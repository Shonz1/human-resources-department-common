import {EmployeeId} from "./employee.entity";

export type CompanyId = string;

export class CompanyEntity {
    constructor(
        private readonly _id: CompanyId,
        private readonly _shortName: string,
        private readonly _fullName: string,
        private readonly _internationalName: string,
        private readonly _address: string,
        private readonly _phone: string,
        private readonly _director?: EmployeeId,
    ) {}

    get id(): CompanyId {
        return this._id;
    }

    get shortName(): string {
        return this._shortName;
    }

    get fullName(): string {
        return this._fullName;
    }

    get internationalName(): string {
        return this._internationalName;
    }

    get address(): string {
        return this._address;
    }

    get phone(): string {
        return this._phone;
    }

    get director(): EmployeeId {
        return this._director;
    }
}
