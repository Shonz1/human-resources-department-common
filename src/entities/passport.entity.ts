import {PersonId} from "./person.entity";

export type PassportId = string;

export class PassportEntity {
    constructor(
        private readonly _id: PassportId,
        private readonly _personId: PersonId,
        private readonly _serial: string,
        private readonly _number: string,
        private readonly _issuer: string,
        private readonly _issuedAt: Date,
    ) {}

    get id(): PassportId {
        return this._id;
    }

    get personId(): PersonId {
        return this._personId;
    }

    get serial(): string {
        return this._serial;
    }

    get number(): string {
        return this._number;
    }

    get issuer(): string {
        return this._issuer;
    }

    get issuedAt(): Date {
        return this._issuedAt;
    }
}
