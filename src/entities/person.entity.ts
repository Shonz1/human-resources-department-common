export type PersonId = string;

export class PersonEntity {
    constructor(
        private readonly _id: PersonId,
        private readonly _lastName: string,
        private readonly _firstName: string,
        private readonly _patronymic: string,
        private readonly _gender: boolean,
        private readonly _birthday: Date,
    ) {}

    get id(): PersonId {
        return this._id;
    }

    get lastName(): string {
        return this._lastName;
    }

    get firstName(): string {
        return this._firstName;
    }

    get patronymic(): string {
        return this._patronymic;
    }

    get gender(): boolean {
        return this._gender;
    }

    get birthday(): Date {
        return this._birthday;
    }
}
